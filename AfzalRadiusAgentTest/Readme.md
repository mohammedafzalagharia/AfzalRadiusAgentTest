The assignment solution follows the Model View Controller (MVC) design pattern. 

The assignment solution utilizes the following third-party frameworks/libraries:

Alamofire - Alamofire is used for handling network requests and simplifying API interactions. It provides a convenient and expressive syntax for making HTTP requests, handling responses, and working with JSON data.
KRProgressHUD - KRProgressHUD is used for displaying progress indicators and notifications during network requests or other long-running tasks. It provides a customizable and easy-to-use interface for showing loading spinners, success messages, and error alerts.
