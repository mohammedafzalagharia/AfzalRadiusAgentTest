//
//  ViewController.swift
//  AfzalRadiusAgentTest
//
//  Created by Mohammed Afzal on 08/07/23.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func btnRun(_ sender: UIButton)
    {
        let gotoDetail = self.storyboard?.instantiateViewController(withIdentifier: "DetailVC") as! DetailVC
        self.navigationController?.pushViewController(gotoDetail, animated: true)
    }
    
}

