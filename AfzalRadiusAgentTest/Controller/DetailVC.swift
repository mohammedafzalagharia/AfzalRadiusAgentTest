//
//  DetailVC.swift
//  AfzalRadiusAgentTest
//
//  Created by Mohammed Afzal on 08/07/23.
//

import UIKit
import Alamofire
import KRProgressHUD

class DetailVC: UIViewController {
    
    @IBOutlet weak var MainCollectionView: UICollectionView!
    
    var mainData = [Facility]()
    var exceptions = [[Exclusion]]()
    
    
    var selectedOptions: [String: String] = [:]  // Dictionary to track selected options
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getDataFromApi()
        
        self.MainCollectionView!.register(MyCollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: MyCollectionReusableView.reuseIdentifierHeader)
    }
    
    func getDataFromApi()
    {
        KRProgressHUD.show()
        AF.request(baseURL, method: .get, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
                
            case .failure(let error):
                print(error.localizedDescription)
                KRProgressHUD.dismiss()
                
            case .success(_):
                do{
                    let result = try JSONDecoder().decode(ResponseModel.self , from: response.data!)
                    self.mainData = result.facilities
                    self.exceptions = result.exclusions
                    
                    for i in 0..<self.mainData.count {
                        self.mainData[i].selectedIndex = -1
                        
                    }
                    DispatchQueue.main.async {
                        self.MainCollectionView.reloadData()
                        KRProgressHUD.dismiss()
                    }
                }
                catch{
                    print(error.localizedDescription)
                    KRProgressHUD.dismiss()
                }
            }
        }
        
    }
    
    @IBAction func btnBack(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    // Function to check if an option should be excluded based on the selected options
    func isOptionExcluded(_ option: Option) -> Bool {
        for exclusion in exceptions {
            let facilityID = exclusion[0].facility_id
            let optionsID = exclusion[0].options_id
            
            if let selectedOptionID = selectedOptions[facilityID], selectedOptionID == optionsID {
                for exclusionOption in exclusion {
                    if exclusionOption.facility_id == exclusionOption.facility_id && exclusionOption.options_id == option.id {
                        return true
                    }
                }
            }
        }
        return false
    }
}

extension DetailVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.mainData.count
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.mainData[section].options.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DetailCell", for: indexPath)  as! DetailCell
        
        cell.bgView.backgroundColor = self.mainData[indexPath.section].selectedIndex == indexPath.row ? UIColor(red: 52/255, green: 199/255, blue: 89/255, alpha: 0.65) : UIColor.white
//        UIColor(red: 229/255, green: 229/255, blue: 234/255, alpha: 1)
        
        cell.lblName.text = self.mainData[indexPath.section].options[indexPath.row].name
        cell.imgPoster.image = UIImage(named: self.mainData[indexPath.section].options[indexPath.row].icon)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let facility = mainData[indexPath.section]
        let option = facility.options[indexPath.row]
        
            // Check if the selected option should be excluded
            if isOptionExcluded(option) {
                // Handle the exclusion, e.g., display an error message
                if indexPath.section != 0
                {
                    self.view.makeToast("These option is not available",position: .top)
                }
                
            } else {
                // Update the selected options dictionary
                selectedOptions[facility.facility_id] = option.id
                
                self.mainData[indexPath.section].selectedIndex = indexPath.row
                self.MainCollectionView.reloadData()
            }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 65)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: MyCollectionReusableView.reuseIdentifierHeader, for: indexPath) as! MyCollectionReusableView
        header.setTitle(self.mainData[indexPath.section].name)
        
        return header
    }
    
}

final class MyCollectionReusableView: UICollectionReusableView {
    
    static let reuseIdentifierHeader = "MyId"
    private let titleLabel = UILabel()
    private let view = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        // Configure the title label
        titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
        titleLabel.textColor = .black
        view.backgroundColor = .black
        addSubview(titleLabel)
        addSubview(view)
        
        // Customize the layout of the title label using constraints or frames
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 5),
            titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -15),
            view.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 2),
            view.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            view.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            view.heightAnchor.constraint(equalToConstant: 0.5)
        ])
    }
    
    func setTitle(_ title: String) {
        titleLabel.text = title
    }
    
}

