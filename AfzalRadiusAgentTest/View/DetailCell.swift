//
//  DetailCell.swift
//  AfzalRadiusAgentTest
//
//  Created by Mohammed Afzal on 08/07/23.
//

import UIKit

class DetailCell: UICollectionViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgPoster: UIImageView!
}
