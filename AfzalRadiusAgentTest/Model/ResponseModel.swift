//
//  ResponseModel.swift
//  AfzalRadiusAgentTest
//
//  Created by Mohammed Afzal on 08/07/23.
//

import Foundation

struct ResponseModel: Codable {
    let facilities: [Facility]
    let exclusions: [[Exclusion]]
}

// MARK: - Facility
struct Facility: Codable {
    let facility_id, name: String
    var selectedIndex: Int?
    let options: [Option]
    
}

// MARK: - Option
struct Option: Codable {
    let name, icon, id: String
}

// MARK: - Exclusion
struct Exclusion: Codable {
    let facility_id, options_id: String
    
}
